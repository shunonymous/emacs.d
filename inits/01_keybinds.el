;;; Moving buffer by M-<Arrow-key>
(windmove-default-keybindings)

;;; Switch between frame
(global-set-key (kbd "<S-return>") 'other-frame)

